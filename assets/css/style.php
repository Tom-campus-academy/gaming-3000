html {
  font: italic small-caps bold 16px/2 cursive;
}

.menu {
  margin-left: 10em;
}

.menu li a {
  margin-left: 4em;
}

.titre {
  position: absolute;
  margin-bottom: 4em;
}

.header {
  padding: 20px 10px;
  background-color: RGB(202, 215, 224);
  height: 110px;
  border: solid 2px;
}

.header a {
  float: left;
  color: black;
  text-align: center;
  padding: 12px;
  text-decoration: none;
  font-size: 18px;
}

.logo {
  width: 80px;
  height: 80px;
  margin-left: 1em;
}

nav{
  width: 100%;
  margin: 0 auto;
  background-color: white;
}

nav ul{
  list-style-type: none;
}

nav ul section li{
  float: left;
  text-align: center;
  position: relative;
}

nav a:hover{
  color: RGB(51, 173, 255);
  border-bottom: 2px solid RGB(51, 201, 255);
}

.sous{
  display: none;
  box-shadow: 0px 1px 2px #CCC;
  background-color: white;
  z-index: 1000;
}

nav > ul section li:hover .sous{
  display: block;
}

.sous li{
  float: none;
  width: 100%;
  text-align: left;
}


.sous a:hover{
  border-bottom: none;
  background-color: RGBa(200,200,200,0.1);
}

.deroulant > a::after{
  content:" ▼";
  font-size: 12px;
}

h1 {
  text-align: center;
  text-decoration: underline;
}

.cadre_formation {
  margin-left: auto;
  margin-right: auto;
  width: 90%;
  border: solid 2px;
  border-radius: 10px;
  box-shadow: 4px 4px 4px #CCC ;
  overflow: hidden;
  position: relative;
}

.texte_formation {
  text-align: center;
  font-weight: bold;
}

.scene_esport {
  background-repeat:no-repeat;
  filter : blur(3px);
  z-index: -1;
  position: absolute;
  bottom: 0;
  opacity: 0.5;
}

.ligne2 {
	display: grid;
  grid-template-columns: 50% 50%;
  margin-top: 2em;
  margin-bottom: 2em;
}

.ligne4 {
	display: grid;
  grid-template-columns: 25% 25% 25% 25%;
  margin-bottom: 5em;
}

.ligne3 {
	display: grid;
  grid-template-columns: 33% 33% 33%;
  margin-bottom: 2em;
}

.icone img {
  width: 100px;
  height: 100px;
}

p {
  z-index: 10;
  }

.composant {
  background-color: RGB(202, 215, 224);
  background-size: contain;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  width: 60%;
  margin: 20px auto;
  border: solid 2px;
  border-radius: 10px;
  box-shadow: 4px 4px 4px #CCC ;
}

.position_composant {
  display: flex;
  flex-direction: column;
  text-align: center;
  margin: 25px 35px;
}

.icone_composant img {
  width: 180px;
  height: 150px;
  cursor: pointer;
}

.icone_proco img {
  width: 150px;
  height: 150px;
  cursor: pointer;
}

.icone_ssd img {
  width: 200px;
  height: 150px;
  cursor: pointer;
}

.accueil_formation {
  margin-top: 3em;
}

.accueil_formation h2{
  text-decoration: underline;
}

.cadre_presentation_formation {
  border: solid 2px;
  border-radius: 10px;
  box-shadow: 4px 4px 4px #CCC ;
  margin-left: 2em;
  padding-top: 2em;
}

a:hover {
color: RGB(51, 201, 255);
text-decoration: none;
}

footer {
  postion: bottom;
}

footer p {
  text-align: center;
}

footer img {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 350px;
  height: 100px;
  margin-bottom: 2em;
}

.separation {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 600px;
  height: 120px;
}

.position_peripherique {
  display: flex;
  flex-direction: column;
  text-align: center;
  margin: 25px 35px;
}

.peripherique {
  background-color: RGB(202, 215, 224);
  background-size: contain;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  width: 60%;
  margin: 20px auto;
  border: solid 2px;
  border-radius: 10px;
  box-shadow: 4px 4px 4px #CCC ;
}

.icone_peripherique img {
  width: 180px;
  height: 150px;
  cursor: pointer;
}

.icone_clavier img {
  width: 220px;
  height: 150px;
  cursor: pointer;
}

.cadre_produit {
  margin-left: auto;
  margin-right: auto;
  width: 90%;
  border: solid 2px;
  border-top: 0px;
  box-shadow: 4px 4px 4px #CCC ;
  overflow: hidden;
  position: relative;
}

.dimension_produit {
  width: 180px;
  height: 150px;
  cursor: pointer;
  margin-left: 20px;
  margin: 10px;
  float: left;
}

.cadre_reduc {
  width: 100%;
  height: 34px;
  border: solid 1px;
  overflow: hidden;
  position: relative;
  background-color: black;
}

.texte_reduc {
  color: white;
}

.desc_prod {
  display: flex;
  flex-direction: column;
  padding: 10px;
  float: left;
}

.desc_prod {
 display: flex;
 flex-direction: column;
 padding: 10px;
 align: right;
}

.prix_prod div {
  text-align: right;
  padding: 50px;
}


.formulaire_inscription {
  text-align: center;
}

.footer {
  text-align: center;
  vertical-align: bottom;
}