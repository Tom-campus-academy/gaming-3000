<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <title>Acceuil</title>
    <link rel="stylesheet" href=".\assets\css\style.css">


  </head>

  <body class ="page">

<?php include("header.php"); ?>
<?php include("db_config.php"); ?>
<style type="text/css">
  <?php include('.\assets\css\style.php'); ?>
</style>
<?php


  if(isset($_POST['forminscritpion']))
  {
    $pseudo = htmlspecialchars($_POST['pseudo']);
    $mail = htmlspecialchars($_POST['mail']);
    $mail2 = htmlspecialchars($_POST['mail2']);
    $mdp = sha1($_POST['mdp']);
    $mdp2 = sha1($_POST['mdp2']);

    if(!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']) AND !empty($_POST['mdp']) AND !empty($_POST['mdp2']))
    {


      $pseudolenght = strlen($pseudo);
      if($pseudolenght <= 255)
      {
        if($mail == $mail2)
        {
          if(filter_var($mail, FILTER_VALIDATE_EMAIL))
          {
            $reqmail = $conn->prepare("SELECT * FROM membres WHERE mail = ?");
            $reqmail->execute(array($mail));
            $mailexist = $reqmail->rowCount();
            if($mailexist == 0)
            {
              if($mdp == $mdp2)
              {
                $insertmbr = $conn->prepare("INSERT INTO membres(pseudo, mail, motdepasse) VALUES(?, ?, ?)");
                $insertmbr->execute(array($pseudo, $mail, $mdp));
                $erreur = "Votre compte a bien été crée ! <a href=\"connexion.php\">Me connecter</a>";
              }
              else
              {
                $erreur = "Vos mots de passes ne correspondent pas";
              }
            }
            else
            {
              $erreur = "Adresse mail déjà utilisée !";
            }
          }
          else
          {
            $erreur = "Votre adresse mail n'est pas valide";
          }
        }
        else
        {
          $erreur = "Vos adresses mail ne correspondent pas";
        }
      }
      else
      {
        $erreur = "Votre pseudo ne doit pas contenir plus de 255 caratères";
      }
    }
    else
    {
      $erreur = "Tout les champs doivent être complétés !";
    }
  }

  ?>

<h1>Création de compte</h1>
<div class="formulaire_inscription">
<form action="" method="POST">
    <label for ="pseudo">Pseudo : </label>  <input type="text" name="pseudo" placeholder="Votre pseudo" id="pseudo" value = "<?php if(isset($pseudo)) { echo $pseudo;} ?>"><br />
    <label for ="mail">Mail : </label>  <input type="email" name="mail" placeholder="Votre mail" id="mail" value = "<?php if(isset($mail)) { echo $mail;} ?>"><br />
    <label for ="mail2">Confirmation du mail : </label>  <input type="email" name="mail2" placeholder="Confirmez votre mail" id="mail2" value = "<?php if(isset($mail2)) { echo $mail2;} ?>"><br />
    <label for ="mdp">Mot de passe : </label>  <input type="password" name="mdp" placeholder="Votre mot de passe" id="mdp"><br />
    <label for ="mdp2">Confirmation du mot de passe : </label>  <input type="password" name="mdp2" placeholder="Confirmer votre mdp" id="mdp2"><br />
    <input type="submit" name="forminscritpion" value="Je m'inscris">
</form>
<div>
<br />
<?php

if (isset($erreur))
{
  echo '<font color="red">'. $erreur. '</font>'; //le texte est en rouge
}

?>


    <footer>
    <?php include ("footer.php"); ?>
    </footer>

  </body>
</html>
