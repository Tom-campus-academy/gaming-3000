<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <title>Acceuil</title>

  </head>

  <body class ="page">

  <?php include("header.php");?>
        
  <style type="text/css">
    <?php include('.\assets\css\style.php'); ?>
  </style>
    <main>
      <section class= "accueil_formation">
        <article class="texte_formation">
          <h2>Formation</h2>
          <div class="ligne3">
            <div class="cadre_presentation_formation">
              <a href="formation_esport.php">
              <div class="icone">
                <img src="./assets/images/manette.png" alt="manette"/>
                <p>Esport</p>
              </a>
              </div>
            </div>
            <div class="cadre_presentation_formation">
              <a href="formation_streamer.php">
              <div class="icone">
                <img src="./assets/images/micro.png" alt="micro"/>
                <p>Streamer</p>
              </a>
              </div>
            </div>
            <div class="cadre_presentation_formation">
              <a href="formation_youtuber.php">
              <div class="icone">
                <img src="./assets/images/camera.png" alt="caméra"/>
                <p>Youtuber</p>
              </a>
              </div>
            </div>
          </div>
        </article>
      </section>
      <img class="separation" src="./assets/images/trait_bleu.png" alt="séparation"/>
      <section class="accueil_peripherique">
        <article class="texte_formation">
        <div class="ligne2">
            <div class="cadre_presentation_formation">
            <a href="peripheriques.php">
              <div class="icone">
                <img src="./assets/images/ecran_rouage.png" alt="Périphériques"/>
                <p>Périphériques</p>
              </a>
              </div>
            </div>
            <div class="cadre_presentation_formation">
              <a href="composants.php">
              <div class="icone">
                <img src="./assets/images/cle_molette.png" alt="Composants"/>
                <p>Composants</p>
              </a>
              </div>
            </div>
        </article>
      </section>
      <img class="separation" src="./assets/images/trait_rouge.png" alt="séparation"/>
    </main>
    <footer>
    <?php include ("footer.php"); ?>
    </footer>
  </body>
</html>
