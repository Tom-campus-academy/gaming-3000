<?php
    session_start();
    $bdd = new PDO('mysql:host=127.0.0.1;dbname=espace_membres', 'root', '');

    if(isset($_GET['id']) AND $_GET['id'] > 0)
    {
        $getid = intval($_GET['id']); //Sécurise la variable id
        $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');
        $requser->execute(array($getid));
        $userinfo = $requser->fetch();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Profil</title>
		<meta charset="utf-8">
	</head>
	<body>
    <style type="text/css">
      <?php include('.\assets\css\style.php');
            include("header.php"); ?>
    </style>
    <h1>Profil de <?php echo $userinfo['pseudo']; ?></h1>
    <br />
    <div class = "formulaire_inscription">
    Pseudo = <?php echo $userinfo['pseudo']; ?>
    <br />
    Mail = <?php echo $userinfo['mail']; ?>
    <br />

    <?php
    if(isset($_SESSION['id']) AND $userinfo['id'] == $_SESSION['id'])
    {
    ?>
    <a href="editionprofil.php">Editer mon profil</a>
    <a href="deconnexion.php">Se déconnecter</a>
    <?php
    }
    ?>
    </div>
	</body>
    <footer>
    	<?php include ("footer.php"); ?>
  	</footer>
</html>
<?php
}
?>
