<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Periphériques</title>

		<script src="script.js"></script>

		<?php include("header.php"); ?>
		<style type="text/css">
			<?php include('.\assets\css\style.php'); ?>
		</style>
	</head>
	<body>

		<div class= "peripherique">

    		<div class= "position_peripherique">
        		<div class="icone_peripherique">
				<a href="ecran.php">
        			<img src="./assets/images/ecrang.png" alt="Image_ecran">
        		</div>
        		<h3>Écrans</h3>
				</a>
      		</div>

     		<div class= "position_peripherique">
    			<div class="icone_peripherique">
				<a href="souris.php">
        			<img src="./assets/images/souris.png" alt="Image_souris">
        		</div>
        		<h3>Souris</h3>
				</a>
      		</div>

      		<div class= "position_peripherique">
        		<div class="icone_peripherique">
				<a href="casque.php">
        			<img src="./assets/images/casque.png" alt="Image_casque">
        		</div>
        		<h3>Casques</h3>
				</a>
      		</div>

			<div class= "position_peripherique">
				<div class="icone_peripherique">
				<a href="micro.php">
					<img src="./assets/images/microg.png" alt="Image_micro">
				</div>
				<h3>Micros</h3>
				</a>
			</div>

			<div class= "position_peripherique">
				<div class="icone_peripherique">
				<a href="webcam.php">
					<img src="./assets/images/webcamg.png" alt="Image_webcam">
				</div>
				<h3>Webcams</h3>
				</a>
			</div>

			<div class= "position_peripherique">
				<div class="icone_clavier">
				<a href="clavier.php">
					<img src="./assets/images/clavier.png" alt="Image_clavier">
				</div>
				<h3>Clavier</h3>
				</a>
			</div>
		</div>



	</body>
	<footer>
    <?php include ("footer.php"); ?>
    </footer>
</html>
