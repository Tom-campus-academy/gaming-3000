<?php
    session_start();
    
    $bdd = new PDO('mysql:host=127.0.0.1;dbname=espace_membres', 'root', '');

    if(isset($_SESSION['id']))
    {
        $requser = $bdd->prepare("SELECT * FROM membres WHERE id= ?");
        $requser->execute(array($_SESSION['id']));
        $user = $requser->fetch();

        if(isset($_POST['newpseudo']) AND !empty($_POST['newpseudo']) AND $_POST['newpseudo'] != $user['pseudo'])
        {
            $newpseudo = htmlspecialchars($_POST['newpseudo']);
            $insertpseudo = $bdd->prepare("UPDATE membres SET pseudo = ? WHERE id = ?");
            $insertpseudo->execute(array($newpseudo, $_SESSION['id']));
            header('Location: profil.php?id='.$_SESSION['id']);
        }

        if(isset($_POST['newmdp1']) AND !empty($_POST['newmdp1']) AND isset($_POST['newmdp2']) AND !empty($_POST['newmdp2']))
        {
            $mdp1 = sha1($_POST['newmdp1']);
            $mdp2 = sha1($_POST['newmdp2']);

            if($mdp1 == $mdp2)
            {
                $insertmdp = $bdd->prepare('UPDATE membres SET motdepasse = ? WHERE id = ?');
                $insertmdp->execute(array($mdp1, $_SESSION['id']));
                header('Location: profil.php?id='.$_SESSION['id']);
            }
            else
            {
                $msg = "Vos deux mots de passes ne correspondent pas !";
            }
        }

        if(isset($_POST['newpseudo']) AND $_POST['newpseudo'] == $user['pseudo'])
        {
            header('Location: profil.php?id='.$_SESSION['id']);
        }

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Edition du profil</title>
        <meta charset="utf-8">
        <style type="text/css">
            <?php include('.\assets\css\style.php');
                include("header.php"); ?>
        </style>
	</head>
	<body>
    
    <h1>Edition de mon profil</h1>
    <form method="POST" action="">
        <label for="">Pseudo :</label>
        <input type="text" name="newpseudo" placeholder="Pseudo" value="<?php echo $user['pseudo']; ?>" /><br /><br />
        <label for="">Mail :</label>
        <input type="mail" name="newmail" placeholder="Mail" value="<?php echo $user['mail']; ?>" /><br /><br />
        <label for="">Mot de passe :</label>
        <input type="password" name="newmdp1" placeholder="Mot de passe" /><br /><br />
        <label for="">Confirmation mot de passe :</label>
        <input type="password" name="newmdp2" placeholder="Confirmation mot de passe" /><br /><br />
        <input type="submit" value="Mettre a jour mon profil">
    </form>
        <?php if(isset($msg)){ echo $msg;} ?>
	</body>
    <footer>
    <?php include ("footer.php"); ?>
    </footer>
</html>
<?php
}
else
{
    header("Location: connexion.php");
}
?>
