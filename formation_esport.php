<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Formation Esport</title>
  <script src="script.js"></script>
</head>
<body>

  <?php include("header.php"); ?>
  <style type="text/css">
    <?php include('.\assets\css\style.php'); ?>
  </style>
  <h1>Formation Esport</h1>
  <section class="cadre_formation">
    <img class="scene_esport" src="./assets/images/scene_esport.jpg" alt="banniere esport">
    <article class="texte_formation">
      <h2>ESPORT</h2>
      <div class="ligne4">
        <div class="icone">
          <img src="./assets/images/podium.png" alt="icone podium"/>
          <p>•Des intervenants du monde <br>
            de l'esport</p>
        </div>
        <div class="icone">
          <img src="./assets/images/joueur.png" alt="icone joueur"/>
          <p>•Développer votre réseau <br>
           de professionnels de l'esport</p>
        </div>
        <div class="icone">
          <img src="./assets/images/affrontement.png" alt="icone affrontement"/>
          <p>•Créer des projets autour <br>
           de l'esport</p>
        </div>
        <div class="icone">
          <img src="./assets/images/arene.png" alt="icone arene"/>
          <p>•Apprenez les métiers professionnels <br>
            autour de l'esport</p>
        </div>
      </div>
    </article>
  </section>
</body>
<footer>
    <?php include ("footer.php"); ?>
  </footer>
</html>
