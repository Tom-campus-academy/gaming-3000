<!DOCTYPE html>
<html>
	<head>
		<title>Cartes Graphiques</title>
		<meta charset="utf-8">
	</head>
	<body>
		
		<?php include("header.php");?>
		<h1>Cartes Graphiques</h1>

<?php 
$servername = "localhost";
$username = "root";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE DATABASE IF NOT EXISTS produits";
  // On execute la requete de connection à la base de données
  $conn->exec($sql);
  //echo "Base de données produits crée avec succès<br>";
} catch(PDOException $e) {
  //echo $sql . "<br>" . $e->getMessage();
}

try {
    $conn = new PDO("mysql:host=$servername;dbname=produits", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Utilisateur connecté a la base de donnée produits" . "<br>";
  } catch(PDOException $e) {
    //echo "Connection à la base de données produits impossible: " . $e->getMessage();
  }


try {
$connexion = new PDO('mysql:host=localhost;dbname=produits', 'root', '');

$requete = 'SELECT * FROM cg';
$resultat = $connexion->query($requete);
while ($ligne = $resultat->fetch()) {

?>
		<style type="text/css">
			<?php include('.\assets\css\style.php'); ?>
		</style>
			
				<section class="cadre_produit">
					<div class="cadre_reduc">
						<div class="texte_reduc">30% de remise avec le Black Friday!</div>
					</div>
					<div>
						<img class="dimension_produit" src=<?php echo $ligne["photo"]; ?> alt="image_alim">
					</div>
					<div class="desc_prod">
						<h2><?php echo $ligne["nomproduit"];?></h2>
						<div><?php echo $ligne["descriptionproduit"];?></div>
					</div>
					<div class="prix_prod">
						<div><?php echo $ligne["prix"];?> €</div>
						<div>
							<a href="panier.php?action=ajout&amp;l=LIBELLEPRODUIT&amp;q=QUANTITEPRODUIT&amp;p=PRIXPRODUIT" onclick="window.open(this.href, '', 'toolbar=no, location=no, directories=no, status=yes, scrollbars=yes, resizable=yes, copyhistory=no, width=600, height=350'); return false;">Ajouter au panier</a>
						</div>
					</div>
				</section><br></br>
				<?php } } catch (PDOException $e){
				echo "Erreur : " .$e->getMessage() . "<br />";
				die();
			} ?>
				
	</body>
	<footer>
    <?php include ("footer.php"); ?>
    </footer>
</html>