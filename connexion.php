<?php
session_start();
    $bdd = new PDO('mysql:host=127.0.0.1;dbname=espace_membres', 'root', '');

    if(isset($_POST['formconnexion']))
    {
        $mailconnect = htmlspecialchars($_POST['mailconnect']);
        $mdpconnect = sha1($_POST['mdpconnect']);
        if(!empty($mailconnect) AND !empty($mdpconnect))
        {
            $requser =$bdd->prepare("SELECT * FROM membres WHERE mail = ? AND motdepasse = ?");
            $requser->execute(array($mailconnect, $mdpconnect));
            $userexist = $requser->rowCount();
            if($userexist == 1)
            {
                $userinfo = $requser->fetch();
                $_SESSION['id'] = $userinfo['id'];
                $_SESSION['pseudo'] = $userinfo['pseudo'];
                $_SESSION['mail'] = $userinfo['mail'];
                header("Location: profil.php?id=".$_SESSION['id']);
            }
            else
            {
                $erreur = "Mauvais mail ou mot de passe !";
            }
        }
        else
        {
            $erreur = "Tout les champs doivent être complétés !";
        }
    }

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Connexion</title>
		<meta charset="utf-8">
	</head>
	<body>
    <style type="text/css">
      <?php include('.\assets\css\style.php');
            include("header.php"); ?>
    </style>
    <h1>Connexion</h1>
    <div class = formulaire_inscription>
        <form action="" method="POST">
            <input type="email" name="mailconnect" placeholder="Mail"/>
            <input type="password" name="mdpconnect" placeholder="Mot de passe"/>
            <input type="submit" name="formconnexion" value="Se connecter"/>
        </form>
    </div>
    <br />
<?php

if (isset($erreur))
{
    echo '<div class = formulaire_inscription><font color="red">'. $erreur. '</font>';
}

?>

	</body>
</html>
