-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 09 déc. 2020 à 09:32
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `produits`
--
CREATE DATABASE IF NOT EXISTS `produits` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `produits`;

-- --------------------------------------------------------

--
-- Structure de la table `alimentation`
--

DROP TABLE IF EXISTS `alimentation`;
CREATE TABLE IF NOT EXISTS `alimentation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `alimentation`
--

INSERT INTO `alimentation` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(3, 'Corsair AX1600i - 1600W', 'Alimentation PC Certifiee 80+ Titanium - Modulaire - Semi-passive', 570, 'https://www.topachat.com/boutique/img/in/in1011/in10110206/in1011020602@2x.jpg'),
(1, 'Asus ROG THOR - 1200W', 'Alimentation PC Certifiee 80+ Platinum - Modulaire - Semi-passive', 400, 'https://www.topachat.com/boutique/img/in/in1011/in10117696/in1011769602@2x.jpg'),
(2, 'Enermax Platimax - 1700W', 'Alimentation PC Certifiee 80+ Platinum - Modulaire', 370, 'https://www.topachat.com/boutique/img/in/in1009/in10098759/in1009875902@2x.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `casques`
--

DROP TABLE IF EXISTS `casques`;
CREATE TABLE IF NOT EXISTS `casques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `casques`
--

INSERT INTO `casques` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'Audeze Mobius - Carbone', 'Casque-micro gamer Sans fil 7.1 - PC / Xbox / PlayStation', 390, 'https://www.topachat.com/boutique/img/in/in2000/in20006250/in2000625002.jpg'),
(2, 'SteelSeries Arctis Pro Wireless', 'Casque-micro gamer sans fil - PC / Mac/ Xbox One', 350, 'https://www.topachat.com/boutique/img/in/in1011/in10111227/in1011122702.jpg'),
(3, 'HyperX Cloud Orbit S', 'Casque-micro gamer 7.1 - Technologie Wave NX - PC ', 330, 'https://www.topachat.com/boutique/img/in/in1101/in11018206/in1101820602.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `cg`
--

DROP TABLE IF EXISTS `cg`;
CREATE TABLE IF NOT EXISTS `cg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cg`
--

INSERT INTO `cg` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'MSI GeForce GT 710 1GD3H LP', 'Carte graphique PCI-Express - Refroidissement passif', 45, 'https://www.topachat.com/boutique/img/in/in1009/in10095284/in1009528402@2x.jpg'),
(2, 'EVGA GeForce RTX 3090 FTW3 GAMING', 'Carte graphique PCI-Express - Refroidissement semi-passif (mode 0 dB)', 2050, 'https://www.topachat.com/boutique/img/in/in2000/in20005296/in2000529602@2x.jpg'),
(3, 'MSI GeForce RTX 3090 SUPRIM X', 'Carte graphique PCI-Express overclockee - Refroidissement semi-passif (mode 0 dB)', 1980, 'https://www.topachat.com/boutique/img/in/in2000/in20006026/in2000602602@2x.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `clavier`
--

DROP TABLE IF EXISTS `clavier`;
CREATE TABLE IF NOT EXISTS `clavier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` text NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `clavier`
--

INSERT INTO `clavier` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'Corsair K100', 'Clavier Gamer mecanique - Retroeclairage 16.8M de couleurs touche par touche', 250, 'https://www.topachat.com/boutique/img/in/in2000/in20005066/in2000506602.jpg'),
(2, 'Steelseries Apex Pro', 'Clavier Gamer mecanique - Retroeclairage 16.8M de couleurs touche par touche', 230, 'https://www.topachat.com/boutique/img/in/in1101/in11011363/in1101136302.jpg'),
(3, 'Razer Huntsman Elite', 'Clavier Gamer mecanique - Retroeclairage 16.8 millions de couleurs', 210, 'https://www.topachat.com/boutique/img/in/in1011/in10113114/in1011311402.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `cm`
--

DROP TABLE IF EXISTS `cm`;
CREATE TABLE IF NOT EXISTS `cm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cm`
--

INSERT INTO `cm` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'ASRock H470M PRO4', 'Carte mere mATX - Socket 1200 - Chipset Intel H470 - USB 3.1 - SATA 6 Gb/s - M.2 - LEDs integrees', 122, 'https://www.topachat.com/boutique/img/in/in2000/in20002950/in2000295002@2x.jpg'),
(2, 'ASRock H470 Steel Legend', 'Carte mere ATX - Socket 1200 - Chipset Intel H470 - USB 3.1 - SATA 6 Gb/s - M.2 - LED integrees', 180, 'https://www.topachat.com/boutique/img/in/in2000/in20002952/in2000295202@2x.jpg'),
(3, 'ASRock B460 PHANTOM GAMING 4', 'Carte mere ATX - Socket 1200 - Chipset Intel B460 - USB 3.0 - SATA 6 Gb/s - M.2', 200, 'https://www.topachat.com/boutique/img/in/in2000/in20002638/in2000263802@2x.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `disque`
--

DROP TABLE IF EXISTS `disque`;
CREATE TABLE IF NOT EXISTS `disque` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `disque`
--

INSERT INTO `disque` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'Samsung Serie 860 EVO 1 To', 'SSD 2.5\" - SATA III - Controleur Samsung MJX - Lecture max : 550 Mo/s', 160, 'https://www.topachat.com/boutique/img/in/in1010/in10109768/in1010976802.jpg'),
(2, 'Samsung Serie 970 EVO Plus 500 Go', 'SSD M.2 - PCI-Express 3.0 NVMe - Controleur Samsung Phoenix - Lecture max : 3500 Mo/s', 138, 'https://www.topachat.com/boutique/img/in/in1011/in10116807/in1011680702.jpg'),
(3, 'Kingston A400 480 Go', 'SSD 2.5\" - SATA III - Controleur Phison S11 - Lecture max : 500 Mo/s', 70, 'https://www.topachat.com/boutique/img/in/in1010/in10104495/in1010449502@2x.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `ecran`
--

DROP TABLE IF EXISTS `ecran`;
CREATE TABLE IF NOT EXISTS `ecran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ecran`
--

INSERT INTO `ecran` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'ASUS ROG Swift PG65UQ', 'Moniteur 65\" VA 144 Hz - 3840 x 2160 px (UHD 4K) - 4 ms', 4160, 'https://www.topachat.com/boutique/img/in/in2000/in20001430/in2000143002.jpg'),
(2, 'AOC AGON AG353UCG G-Sync', 'Moniteur 35\" VA 21/9 200 Hz - HDR - 3440 x 1440 px (WQHD) - 2 ms ', 2500, 'https://www.topachat.com/boutique/img/in/in2000/in20002689/in2000268902.jpg'),
(3, 'Acer Predator X27P', 'Moniteur 27\" IPS LED 144 Hz - HDR 1000 - 3840 x 2160 px (UHD 4K)', 1860, 'https://www.topachat.com/boutique/img/in/in1101/in11017941/in1101794102@2x.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `memoire`
--

DROP TABLE IF EXISTS `memoire`;
CREATE TABLE IF NOT EXISTS `memoire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `memoire`
--

INSERT INTO `memoire` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'DDR4 HyperX Fury RGB - 16 Go', 'Kit Dual Channel - Memoire DDR4 optimisee Intel et AMD - PC-25600 - LED RGB\r\n', 115, 'https://www.topachat.com/boutique/img/in/in1101/in11018190/in1101819002@2x.jpg'),
(2, 'DDR4 HyperX Fury - 16 Go', 'Kit Dual Channel - Mémoire DDR4 optimisee Intel et AMD - PC-21300 - Low-Profile', 95, 'https://www.topachat.com/boutique/img/in/in1101/in11018183/in1101818302@2x.jpg'),
(3, 'DDR4 G.Skill Aegis - 16 Go', 'Kit Dual Channel - Memoire DDR4 optimisee Intel - PC-25600 - Low-Profile', 95, 'https://www.topachat.com/boutique/img/in/in2000/in20000336/in2000033602.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `micro`
--

DROP TABLE IF EXISTS `micro`;
CREATE TABLE IF NOT EXISTS `micro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` text NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `micro`
--

INSERT INTO `micro` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'Blue Yeti Pro', 'Microphone Gamer - PC - USB - XLR', 280, 'https://www.topachat.com/boutique/img/in/in2000/in20001258/in2000125802.jpg'),
(2, 'Razer Seiren Elite', 'Microphone Gamer - PC - USB', 210, 'https://www.topachat.com/boutique/img/in/in1011/in10110411/in1011041102.jpg'),
(3, 'Samson G-Track Pro', 'Microphone PC - USB', 200, 'https://www.topachat.com/boutique/img/in/in2000/in20000683/in2000068302.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `processeurs`
--

DROP TABLE IF EXISTS `processeurs`;
CREATE TABLE IF NOT EXISTS `processeurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `processeurs`
--

INSERT INTO `processeurs` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'AMD Ryzen 7 3700X (3.6 GHz)', 'Processeur Socket AM4 - Octo Core - Cache 36 Mo - Matisse', 360, 'https://www.topachat.com/boutique/img/in/in1101/in11016968/in1101696802@2x.jpg'),
(2, 'Intel i3-10100F (3.6 GHz)', 'Processeur Socket 1200 - Quad Core - Cache 6 Mo - Comet Lake-S - Ventirad inclus', 110, 'https://www.topachat.com/boutique/img/in/in2000/in20005971/in2000597102@2x.jpg'),
(3, 'Intel Core i9-10850K (3.6 GHz)', 'Processeur Socket 1200 - 10 coeurs - Cache 20 Mo - Comet Lake-S - Ventirad non inclus + en promo + Marvel\'s Avengers offert !', 540, 'https://www.topachat.com/boutique/img/in/in2000/in20004340/in2000434002@2x.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `souris`
--

DROP TABLE IF EXISTS `souris`;
CREATE TABLE IF NOT EXISTS `souris` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` varchar(255) NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `souris`
--

INSERT INTO `souris` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'Asus ROG Chakram', 'Souris Gamer optique - Resolution ajustable jusqu\'a 16 000 dpi', 160, 'https://www.topachat.com/boutique/img/in/in1102/in11020249/in1102024902@2x.jpg'),
(2, 'Razer Basilisk Ultimate', 'Souris Gamer optique sans-fil - Resolution jusqu\'a 20 000 dpi', 180, 'https://www.topachat.com/boutique/img/in/in1101/in11019279/in1101927902@2x.jpg'),
(3, 'Asus ROG Spatha', 'Souris Gamer laser - Resolution ajustable jusqu\'a 8200 dpi', 170, 'https://www.topachat.com/boutique/img/in/in1010/in10100267/in1010026702.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `webcam`
--

DROP TABLE IF EXISTS `webcam`;
CREATE TABLE IF NOT EXISTS `webcam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomproduit` text NOT NULL,
  `descriptionproduit` text NOT NULL,
  `prix` int(11) NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `webcam`
--

INSERT INTO `webcam` (`id`, `nomproduit`, `descriptionproduit`, `prix`, `photo`) VALUES
(1, 'Logitech BRIO Stream', 'Webcam UHD 4K - HDR', 240, 'https://www.topachat.com/boutique/img/in/in1102/in11020089/in1102008902.jpg'),
(2, 'Logitech Webcam C930e', 'Webcam Full HD 1080p - Resolution photo 3 Mpx', 130, 'https://www.topachat.com/boutique/img/in/in1009/in10095559/in1009555902.jpg'),
(3, 'AVerMedia Live Streamer', 'Webcam Full HD 1080p', 125, 'https://www.topachat.com/boutique/img/in/in2000/in20002920/in2000292002.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
