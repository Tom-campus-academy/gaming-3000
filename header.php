<style type="text/css">
  <?php include('.\assets\css\style.php'); ?>
</style>
<header>
    <div class="header">
            <div class="titre">
            <a href = "index.php"><img class="logo" src=".\assets\images\logoG.png" alt="logo"></a>
                <nav>
                    <ul>
                        <section class="menu">
                        <li><a href="index.php">Accueil</a></li>
                        <li class="deroulant"><a href="#">Formation &ensp;</a>
                            <ul class="sous">
                                <li><a href="formation_esport.php">Formation Esport</a></li>
                                <li><a href="formation_streamer.php">Formation Streamer</a></li>
                                <li><a href="formation_youtuber.php">Formation Youtuber</a></li>
                            </ul>
                        </li>
                        <li><a href="peripheriques.php">Périphériques</a></li>
                        <li><a href="composants.php">Composants</a></li>
                        <li><a href="espace_client.php">Espace Client</a></li>
                        </section>
                    </ul>
                </nav>
            </div>
            <script src="script.js"></script>
    </div>
</header>