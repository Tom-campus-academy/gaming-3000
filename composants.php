<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Composants</title>

  <script src="script.js"></script>

  <?php include("header.php"); ?>
  <style type="text/css">
    <?php include('.\assets\css\style.php'); ?>
  </style>

</head>
<body>

    <div class= "composant">
      <div class= "position_composant">
        <div class="icone_composant">
        <a href="alimentation.php">
          <img src="./assets/images/alimentation.png" alt="Image_ali">
        </div>
        <h3>Alimentations</h3>
        </a>
      </div>

      <div class= "position_composant">
        <div class="icone_composant">
        <a href="cartesmere.php">
          <img src="./assets/images/carte_mere.png" alt="Image_cm">
        </div>
        <h3>Cartes mères</h3>
        </a>
      </div>

      <div class= "position_composant">
        <div class="icone_proco">
        <a href="processeurs.php">
          <img src="./assets/images/processeur.png" alt="Image_pro">
        </div>
        <h3>Processeurs</h3></a>
      </div>

      <div class= "position_composant">
        <div class="icone_composant">
        <a href="ram.php">
          <img src="./assets/images/ram.png" alt="Image_ram">
        </div>
        <h3>Mémoires</h3>
        </a>
      </div>

      <div class= "position_composant">
        <div class="icone_ssd">
        <a href="ssd.php">
          <img src="./assets/images/ssd.png" alt="Image_ssd">
        </div>
        <h3>SSD</h3>
        </a>
      </div>

      <div class= "position_composant">
        <div class="icone_composant">
        <a href="cartesgrph.php">
          <img src="./assets/images/cartegraph.png" alt="Image_cg">
        </div>
        <h3>Cartes Graphiques</h3>
        </a>
      </div>
     </div>
</body>
<footer>
    <?php include ("footer.php"); ?>
    </footer>
</html>
