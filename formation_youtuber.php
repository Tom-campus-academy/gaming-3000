<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <title>Formation Esport</title>
  <script src="script.js"></script>
</head>
<body>

  <?php include("header.php"); ?>
  <style type="text/css">
    <?php include('.\assets\css\style.php'); ?>
  </style>
  <h1>Formation Youtuber</h1>
  <section class="cadre_formation">
    <img class="scene_esport" src=".\assets\images\scene_esport.jpg" alt="banniere esport">
    <article class="texte_formation">
      <h2>Youtuber</h2>
      <div class="ligne3">
        <div class="icone">
          <img src="./assets/images/camera.png" alt="icone camera"/>
          <p>•Savoir manier les outils de l'audiovisuel <br>
            •Savoir manier les logiciels de montage</p>
        </div>
        <div class="icone">
          <img src="./assets/images/micro.png" alt="icone micro"/>
          <p>•Avoir fidéliser son public <br>
            •Savoir être créatif/apporter un renouveau <br>
            •Avoir une bonne élocution</p>
        </div>
        <div class="icone">
          <img src="./assets/images/reseau.png" alt="icone reseau"/>
          <p>•Développer ses réseaux <br>
            •Savoir bien référencer sa production <br>
            •Gestion de partenariat</p>
          </div>
      </div>
    </article>
  </section>
</body>
<footer>
    <?php include ("footer.php"); ?>
    </footer>
</html>
