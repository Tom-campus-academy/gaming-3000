<?php

$servername = "localhost";
$username = "root";
$password = "";

try {
  $conn = new PDO("mysql:host=$servername", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $sql = "CREATE DATABASE IF NOT EXISTS espace_membres";
  // On execute la requete de connection à la base de données
  $conn->exec($sql);
  //echo "Base de données espace_membres crée avec succès<br>";
} catch(PDOException $e) {
  echo $sql . "<br>" . $e->getMessage();
}

try {
    $conn = new PDO("mysql:host=$servername;dbname=espace_membres", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Utilisateur connecté a la base de donnée espaces_membres" . "<br>";
  } catch(PDOException $e) {
    echo "Connection à la base de données espace_membres impossible: " . $e->getMessage();
  }

//Création de la table membres avec tout les paramètres (id, pseudo, mail, mot de passe)  
$table_sql = "CREATE TABLE IF NOT EXISTS membres
(id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, 
pseudo VARCHAR(255) NOT NULL, 
mail VARCHAR(255) NOT NULL, 
motdepasse TEXT NOT NULL)";

//Execution de la requete
try {
    $conn->exec($table_sql);
    //echo "La table membres a bien été crée"; 
} catch(PDOException $e) {
    echo $sql . "<br>" . $e->getMessage();
}


$conn = new PDO("mysql:host=$servername;dbname=espace_membres", $username, $password);